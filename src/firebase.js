import firebase from "firebase/app";
import "firebase/firestore";

// Your web app's Firebase configuration
var firebaseConfig = {
	apiKey: "AIzaSyC1Yxm_8KqmzYs_bLKBVvmGeXDg0obAln0",
	authDomain: "reacthooks-firebase-clock.firebaseapp.com",
	databaseURL: "https://reacthooks-firebase-clock.firebaseio.com",
	projectId: "reacthooks-firebase-clock",
	storageBucket: "reacthooks-firebase-clock.appspot.com",
	messagingSenderId: "1069514731679",
	appId: "1:1069514731679:web:f27f351ccf6e13d53ab2aa",
	measurementId: "G-FSTQG7ZZJL",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase;