import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import React, { useState } from "react";


const useStyles = makeStyles(theme => ({
	TextField: {
		display: "flex",
	},
	form: {
		"& .MuiTextField-root": {
			margin: theme.spacing(1),
		},
	},
}));

const AddTimeEntryFrom = ({ onSubmit }) => {
	const classes = useStyles();
	const [title, setTitle] = useState("");
	const [time, setTime] = useState("");

	return (
		<Grid container>
			<Grid item xs={12}>
				<Typography className="title" variant={"h4"}>CRUD</Typography>
			</Grid>
			<Grid item xs={12}>

				<form className={classes.form}
				      onSubmit={(event) => {
					      onSubmit({ event, title, time });
					      setTime("");
					      setTitle("");
				      }}>
					<div>
						<TextField className={classes.TextField}
						           type={"text"}
						           label="Title"
						           required
						           value={title}
						           onChange={e => {
							           setTitle(e.currentTarget.value);
						           }}/>
						<TextField className={classes.TextField}
						           type={"number"}
						           label="Time"
						           required
						           value={time}
						           onChange={e => {
							           setTime(e.currentTarget.value);
						           }}/>
					</div>

					<button>Add time entry</button>
				</form>
			</Grid>
		</Grid>);
};
export default AddTimeEntryFrom;