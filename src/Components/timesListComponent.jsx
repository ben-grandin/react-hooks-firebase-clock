import { InputLabel, Select } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

import { DeleteRounded } from "@material-ui/icons";
import React from "react";


const useStyles = makeStyles(theme => ({
	timeEntry: {
		display: "flex",
		justifyContent: "space-between",
		width: "50%",
	},
	time: {
		color: "lightskyblue",
	},
}));

const TimeListComponent = ({ onDelete, sortTimes, times, sortBy }) => {
	const classes = useStyles();

	return (
		<Grid container>
			<Grid item xs={12}>
				<Typography className="title" variant="h4">Times
					list</Typography>
			</Grid>

			<Grid container>
				<Grid item xs={12}>
					<InputLabel>Sort by: </InputLabel>
					<Select value={sortBy}
					        onChange={e => {
						        sortTimes(e.currentTarget.dataset.value);
					        }}>
						<MenuItem value={"TIME_ASC"}> Time (fastest
							first)</MenuItem>
						<MenuItem value={"TIME_DESC"}> Time (slowest
							first)</MenuItem>
						<MenuItem disabled>---</MenuItem>
						<MenuItem value={"TITLE_ASC"}>Title (a-z)</MenuItem>
						<MenuItem value={"TITLE_DESC"}>Title (z-a)</MenuItem>
					</Select>
				</Grid>

				<Grid item xs={12}>
					<List component="nav"
					      aria-label="secondary mailbox folders">
						{times.map(({ id, title, time_seconds }, index) => (
								<ListItem key={id} button>
									<ListItemText className={classes.timeEntry}
									              primary={`${index +
									              1}.  ${title}`}
									              secondary={<Typography
										              className={classes.time}>{time_seconds} seconds</Typography>
									              }>
									</ListItemText>
									<ListItemSecondaryAction>
										<IconButton edge="end" data-id={id}
										            data-title={title}
										            aria-label="delete"
										            onClick={onDelete}>
											<DeleteRounded/>
										</IconButton>
									</ListItemSecondaryAction>
								</ListItem>

							),
						)}
					</List>
				</Grid>
			</Grid>
		</Grid>
	);
};
export default TimeListComponent;
