import { Container, withStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import React, { Component } from "react";
import "./App.css";
import AddTimeEntryFrom from "./Components/addTimeEntryFormComponent";
import TimeList from "./Components/timesListComponent";
import firebase from "./firebase";


const styles = {
	app: {
		// background: "rgba(4,66,144,0.6)",
		borderRadius: 4,
		background: "#fff",
		maxWidth: "90%",
		margin: "2rem auto",
		padding: "2rem",
		boxShadow: "0 2 4 #ddd",

	},
	container: {
		justifyContent: "space-evenly",
	},
	section: {
		background: "bisque",
		borderRadius: 4,
		textAlign: "-webkit-center",
	},
	"@global": {
		"html": {
			// backgroundImage: "radial-gradient(circle, #f90012, #da005e, #962082, #45387d, #023358)",
			background: "lightslategrey",
			position: "fixed",
			top: 0,
			bottom: 0,
			left: 0,
			right: 0,
		},
		".title": {
			textAlign: "center",
			paddingBottom: "3%",
			// color:"#F90012"
		},
	},
};

const SORT_OPTIONS = {
	"TIME_ASC": { column: "time_seconds", direction: "asc" },
	"TIME_DESC": { column: "time_seconds", direction: "desc" },

	"TITLE_ASC": { column: "title", direction: "asc" },
	"TITLE_DESC": { column: "title", direction: "desc" },
};

const DEFAULT_STATE = {
	times: [],
	sortBy: "TIME_ASC",
};

class App extends Component {
	state = DEFAULT_STATE;

	onSubmit = ({ event, title, time }) => {
		event.preventDefault();
		firebase
			.firestore()
			.collection("times")
			.add({
				title,
				time_seconds: parseInt(time),
			})
			.then(() => {this.sortTimes(this.state.sortBy);});
	};

	onDelete = (e) => {
		e.preventDefault();
		const { id, title } = e.currentTarget.dataset;
		// eslint-disable-next-line no-restricted-globals
		const confirmDelete = confirm(
			`Are you sure that you want to delete: ${title} `);

		if ( confirmDelete ) {
			firebase
				.firestore()
				.collection("times")
				.doc(id)
				.delete()
				.then();
		}
	};

	getTimes = () => {
		const { sortBy } = this.state;

		firebase
			.firestore()
			.collection("times")
			.orderBy(SORT_OPTIONS[sortBy].column,
				SORT_OPTIONS[sortBy].direction)
			.onSnapshot(snapshot => {
				const times = snapshot.docs.map(doc => ({
					id: doc.id,
					...doc.data(),
				}));
				this.setState({ times });
			});

	};

	sortTimes = (sortBy = "TIME_ASC") => {
		const { times } = this.state;

		times.sort((a, b) => {
			const aValue = a[SORT_OPTIONS[sortBy].column];
			const bValue = b[SORT_OPTIONS[sortBy].column];

			switch ( typeof times[0][SORT_OPTIONS[sortBy].column] ) {
				case "number":
					if ( SORT_OPTIONS[sortBy].direction === "asc" ) {
						return aValue - bValue;
					} else {
						return bValue - aValue;
					}

				case "string":
					if ( SORT_OPTIONS[sortBy].direction === "asc" ) {
						return aValue.localeCompare(bValue);
					} else {
						return bValue.localeCompare(aValue);
					}

				default:
					throw new Error("Cannot sort times: wrong type");
			}
		});

		this.setState({ times, sortBy });
	};

	componentDidMount() {
		this.getTimes();
	}

	render() {
		const { classes } = this.props;
		const { times, sortBy } = this.state;

		return (
			<Container className={classes.app}>

				<Grid item xs={12}>
					<Typography className="title"
					            variant="h2">
						Just Clock It
					</Typography>
				</Grid>

				<Grid item xs={12}>
					<Grid className={classes.container} container>
						<Grid item className={classes.section} xs={6}>
							<TimeList times={times}
							          sortBy={sortBy}
							          onDelete={this.onDelete}
							          sortTimes={this.sortTimes}/>
						</Grid>

						<Grid item className={classes.section} xs={5}>
							<AddTimeEntryFrom onSubmit={this.onSubmit}/>
						</Grid>
					</Grid>
				</Grid>

			</Container>
		);
	}
}

export default withStyles(styles)(App);
